<?php

require_once('base.class.php');
require_once(PATH . '/cronjobs/sc-api/GoogleSearchConsoleAPI.class.php');

class scget extends asebase {

  public $sparte = array(
      'leben'   => array('https://www.allianz.de/vorsorge'), 
      'kranken' => array('https://www.allianz.de/gesundheit', 'https://www.allianz.de/reise-und-freizeit'), 
      'sach'    => array('https://www.allianz.de/auto', 'https://www.allianz.de/recht-und-eigentum') 
    );


  public function __construct () {

    $this->sc      = $this->auth();
    $auth          = $this->sc->auth->getAccessToken();

    if ($auth['http_code'] == 200) {
      $accessToken  = $auth['access_token'];
      $tokenExpires = $auth['expires_in'];
      $tokenCreated = time();
    } else {
      parent::logToFile(parent::timeStamp() . ' SC: No auth token created!');
    }

    $this->sc->setAccessToken($accessToken);
    $profiles = $this->sc->getProfiles();

    $this->accounts = array();

    foreach ($profiles['siteEntry'] as $item) {
      if ($item['permissionLevel'] == 'siteUnverifiedUser') {
        continue;
      }
      $this->accounts[] = $item['siteUrl'];
    }


    $date = strtotime('last monday');

    $date2 = date("Y-m-d", $date);
    $d2 = $date - 518400;
    $date1 = date("Y-m-d", $d2);


    $this->queryAPIDevice($date1, $date2);
    $this->queryAPICountry($date1, $date2);
    $this->queryAPIClicks($date1, $date2);

  }


  private function queryAPIDevice($sdate, $edate) {

    $dataset = array ();
    $dataset[]  = $this->sc->getDataDevice('https://www.allianz.de/', $sdate, $edate, 5000, 0);

    $res = array();

    foreach ($dataset as $key => $data) {

      foreach ($data['rows'] as $key => $datas) {

        $device      = $datas['keys'][0];        
        $clicks      = $datas['clicks'];

        $res[$device]   = $clicks;

      }

    }

    parent::mySqlConnect();

    foreach ($res as $key => $clicks) {

      $date        = $edate;
      $device      = $key;

      $sql_ra = "('".$device."', '".$clicks."', '".$date."')";

      $query_ra = 'INSERT INTO
                       aat_searchconsole_device (device, clicks, timestamp)
                    VALUES
                      '. $sql_ra .' ';

      $call = $this->db->query($query_ra);

      if (!empty($this->db->error)) {
        parent::logToFile(parent::timeStamp() . ' SISTRIX INDEXWATCH DATA: DB ERROR: ' . $this->db->error);
      }

    }

    parent::mySqlClose();


  }

  private function queryAPICountry($sdate, $edate) {

    $rows = array (0);

    foreach ($rows as $startRow) {
      $dataset[]  = $this->sc->getDataCountry('https://www.allianz.de/', $sdate, $edate, 5000, $startRow);
    }

    $res = array();

    foreach ($dataset as $key => $data) {

      foreach ($data['rows'] as $key => $datas) {

        $country     = $datas['keys'][0];        
        $clicks      = $datas['clicks'];

        $res[$country]   = $clicks;

      }

    }

    parent::mySqlConnect();

    foreach ($res as $key => $clicks) {

      $date        = $edate;
      $country     = $key;

      $sql_ra = "('".$country."', '".$clicks."', '".$date."')";

      $query_ra = 'INSERT INTO
                       aat_searchconsole_country (country, clicks, timestamp)
                    VALUES
                      '. $sql_ra .' ';

      $call = $this->db->query($query_ra);
      if (!empty($this->db->error)) {
        parent::logToFile(parent::timeStamp() . ' SISTRIX INDEXWATCH DATA: DB ERROR: ' . $this->db->error);
      }

    }

    parent::mySqlClose();

  }


  private function queryAPIClicks($sdate, $edate) {

    $rows = array (0);

    foreach ($rows as $startRow) {
      $dataset[]  = $this->sc->getData('https://www.allianz.de/', $sdate, $edate, 5000, $startRow);
    }

    $res = array();
    $res['all']     = array('clicks' => 0, 'impres' => 0);
    $res['leben']   = array('clicks' => 0, 'impres' => 0);
    $res['kranken'] = array('clicks' => 0, 'impres' => 0);
    $res['sach']    = array('clicks' => 0, 'impres' => 0);

    foreach ($dataset as $key => $data) {

      echo 'DO DS' . PHP_EOL;

      foreach ($data['rows'] as $key => $urls) {

        $url         = $urls['keys'][0];        
        $clicks      = $urls['clicks'];
        $impressions = $urls['impressions'];

        $res['all']['clicks'] = $res['all']['clicks'] + $clicks;
        $res['all']['impres'] = $res['all']['impres'] + $impressions;

        if (stripos($url, $this->sparte['leben'][0]) !== FALSE) {
          $res['leben']['clicks'] = $res['leben']['clicks'] + $clicks;
          $res['leben']['impres'] = $res['leben']['impres'] + $impressions;
        }

        if (stripos($url, $this->sparte['kranken'][0]) !== FALSE || stripos($url, $this->sparte['kranken'][1]) !== FALSE) {
          $res['kranken']['clicks'] = $res['kranken']['clicks'] + $clicks;
          $res['kranken']['impres'] = $res['kranken']['impres'] + $impressions;
        }

        if (stripos($url, $this->sparte['sach'][0]) !== FALSE || stripos($url, $this->sparte['sach'][1])  !== FALSE) {
          $res['sach']['clicks'] = $res['sach']['clicks'] + $clicks;
          $res['sach']['impres'] = $res['sach']['impres'] + $impressions;
        }

      }

    }


    parent::mySqlConnect();

    foreach ($res as $key => $arr) {

      $date        = $sdate;
      $sparte      = $key;
      $clicks      = $arr['clicks'];
      $impressions = $arr['impres'];

      $sql_ra = "('".$sparte."', '".$clicks."', '".$impressions."', '".$date."')";

      $query_ra = 'INSERT INTO
                       aat_searchconsole (sparte, clicks, impressions, timestamp)
                    VALUES
                      '. $sql_ra .' ';

      $call = $this->db->query($query_ra);

      if (!empty($this->db->error)) {
        parent::logToFile(parent::timeStamp() . ' SISTRIX INDEXWATCH DATA: DB ERROR: ' . $this->db->error);
      }

    }

    parent::mySqlClose();

  }


  public function startDate () {
    return date("Y-m-d", strtotime('- 4 days'));
  }


  public function endDate () {
    return date("Y-m-d", strtotime('- 3 days'));
  }


  public function auth () {

    $ga = new GoogleSearchConsoleAPI('service');

    $ga->auth->setClientId('352755655277-f93mdkia0jte33tave8n9b20lo85mo6o.apps.googleusercontent.com');
    $ga->auth->setEmail('352755655277-f93mdkia0jte33tave8n9b20lo85mo6o@developer.gserviceaccount.com');
    $ga->auth->setPrivateKey('/var/www/oneproapi/searchconsole/api/key.p12');

    return $ga;

  }

}

new scget;

?>
