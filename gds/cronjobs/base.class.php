<?php

error_reporting(E_ALL);

date_default_timezone_set('CET');
ini_set('mysql.connect_timeout', 1200);
ini_set('default_socket_timeout', 1200);

define('PATH', dirname(dirname(__FILE__)));
define('STORE', '/store/');
define('LOG', '/log/');
define('WWW', 'https://oneproseo.advertising.de/oneproapi/allianz/gds/');
define('MAILERROR', 'm.hotz@advertising.de');


class asebase {

  public function mySqlConnect () {

    $this->db = new mysqli('192.168.20.2', 'oneproseo', 'zeB3b4SLnZuNu3dT', 'oneproseo_live');
    $this->db->set_charset('utf8');

    if (mysqli_connect_errno()) {
      asebase::logToFile('MYSQL CONNECT ERROR: ' . mysqli_connect_error());
      asebase::alertMail('MYSQL CONNECT ERROR: ' . mysqli_connect_error());
      exit;
    }

  }

  public function mySqlClose () {

    $this->db->close();

  }

  public function reconMySql () {

    if ($this->db->ping() === false) {
      $this->mySqlConnect();
    }

  }

  public function timeStamp () {

    $date = new DateTime();
    $date = $date->getTimestamp();
    return date("h:i / d-m", $date);

  }

  public function logToFile ($content) {

    $file = PATH . LOG . 'aatlog.txt';

    $fh = fopen($file, 'a');
    fwrite($fh, $this->timeStamp() . ' - ' . $content . "\r\n");
    fclose($fh);

  }

  public function alertMail ($content) {

    mail(MAILERROR, 'ASE - Dashboard - Error', $content);

  }

  public function pureHostName ($url) {

    $pure_host  = str_ireplace('www.', '', parse_url($url, PHP_URL_HOST));
    return $pure_host;

  }

  public function dateYMD () {
    $date = new DateTime();
    $date = $date->getTimestamp();
    return date("Y-m-d", $date);
  }

  public function dateYMDyesterday () {   
    return date('Y-m-d', strtotime('yesterday'));
  }

  public function dateYMDmondayoneweek () {   
    return date('Y-m-d', strtotime('-1 week monday'));
  }

  public function dateYMDmondaytwoweek () {   
    return date('Y-m-d', strtotime('-2 week monday'));
  }

  public function dateYMDoneweekback () {
    $d = strtotime('yesterday');
    return date('Y-m-d', strtotime('-1 week', $d));
  }

  public function dateYMDoneyearkback () {
    $d = strtotime('yesterday');
    return date('Y-m-d', strtotime('-1 year', $d));
  }

  public function dateHIS () {
    $date = new DateTime();
    $date = $date->getTimestamp();
    return date("h:i:s", $date);
  }


}

?>
