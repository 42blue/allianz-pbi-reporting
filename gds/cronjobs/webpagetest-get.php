<?php


require_once('base.class.php');

class pagespeedget extends asebase {

  public $checks = array(
      'Startseite' => 'www.allianz.de',
      'Produktseite' => 'www.allianz.de/auto/kfz-versicherung/',
      'Kampagnen Landingpage' => 'www.allianz.de/angebot/vorsorge/berufsunfaehigkeit/'
  );


  public function __construct () {

    $this->getPageSpeedApi();
       
  }


  private function getPageSpeedApi ()
  {

    foreach ($this->checks as $name => $url) {

			$date = parent::dateYMD();

      $xml  = $this->readResponse($response, $name);
      $data = new SimpleXMLElement($xml);

      $resultUrl = $data->data->{'jsonUrl'};

      $ctx      = stream_context_create(array('http' => array( 'timeout' => 120 )));
      $response = file_get_contents($resultUrl, false, $ctx);

      $response = json_decode($response, true);

      if ( strripos($response['statusText'], 'test complete') !== false ) {

        $ttfb  = $response['data']['average']['firstView']['TTFB'] / 1000;
        $fmp   = $response['data']['average']['firstView']['firstMeaningfulPaint'] / 1000;
        $tti   = $response['data']['average']['firstView']['domInteractive'] / 1000;
        $score = $response['data']['average']['firstView']['SpeedIndex'] / 1000;

        parent::mySqlConnect();
        
        $sql_ra = "('".$name."', '".$url."', '".$ttfb."', '".$fmp."', '".$tti."', '".$score."', '".$date."')";

        $query_ra = 'INSERT INTO
                         aat_pagespeed (name, url, ttfb, fmp, tti, score, timestamp)
                      VALUES
                        '. $sql_ra .' ';

        $sql = $this->db->query($query_ra);

        parent::mySqlClose();

      } else {

        parent::logToFile(parent::timeStamp() . ' Webpagetest No Result: ' . $resultUrl);

      }

    }

  }


  public function readResponse ($content, $filename) {

    $filename = strtolower($filename);
    $filename = str_replace(' ', '-', $filename);

    $file = PATH . '/cronjobs/webpagetest-api/' . $filename . '.txt';

    $data = file_get_contents($file);

    return $data;

  }


}

new pagespeedget;

?>
