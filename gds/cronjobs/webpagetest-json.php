<?php

require_once('base.class.php');

class pagespeedjson extends asebase {

  public function __construct () {

    parent::mySqlConnect();

    $data = $this->getPageSpeed();
    
    $this->jsonPageSpeed($data);
    $this->jsonPageSpeedDetail($data);

    parent::mySqlClose();

  }


  public function jsonPageSpeed ($data) {

    $result = array();

    foreach ($data as $key => $value) {

      $name  = $value['name'];
      $url   = $value['url'];
      $fmp   = $value['fmp'];
      $ts    = $value['timestamp'];

      $site = $name . ': ' . $url;

      $result[] = array('cw' => date("W", strtotime($ts)), 'site' => $site, 'fmp' => floatval ($fmp));

    }

    $json      = array('results' => $result);
    $jsonfinal = json_encode($json, JSON_PRETTY_PRINT);
    
    $this->writeJson('pagespeed-fmp', $jsonfinal);

  }


  public function jsonPageSpeedDetail ($data) {

    $startDate  = strtotime('monday -2 week');
    $endDate    = strtotime('last monday');
    $first_date = date("Y-m-d", $endDate);
    $last_date  = date("Y-m-d", $startDate);

    krsort($data);

    $result = array();

    foreach ($data as $key => $value) {

      $name  = $value['name'];
      $url   = $value['url'];
      $ttfb  = $value['ttfb'];
      $fmp   = $value['fmp'];
      $tti   = $value['tti'];
      $ts    = $value['timestamp'];

      $site = $url;

      if ($value['timestamp'] == $first_date) {
       
        $result[$site] = array(
          'Seite'                             => $site,
          'TTFB aktuell'     => round(floatval($ttfb), 2),
          'TTFB Vorwoche'                  => 0,
          'FMP aktuell' => round(floatval($fmp), 2),
          'FMP Vorwoche'                   => 0,
          'TTI aktuell'    => round(floatval($tti), 2),
          'TTI Vorwoche'                   => 0
        );

      }

      if ($value['timestamp'] == $last_date) {

        $result[$site]['TTFB Vorwoche'] = round(floatval($ttfb), 2);
        $result[$site]['FMP Vorwoche']  = round(floatval($fmp), 2);
        $result[$site]['TTI Vorwoche']  = round(floatval($tti), 2);

      }

    }

    $result = array_values($result);
    $json      = array('results' => $result);

    $jsonfinal = json_encode($json, JSON_PRETTY_PRINT);
    
    $this->writeJson('pagespeed-detail', $jsonfinal);

  }


  public function getPageSpeed ()
  {

    $sql = "SELECT 
              *
            FROM
              aat_pagespeed
            WHERE 
              DATE(timestamp) > CURDATE() - INTERVAL 12 MONTH";

    $res = $this->db->query($sql);

    $data = array();

    while ($row = $res->fetch_assoc()) {
      $data[] = $row;
    }

    return $data;

  }


  public function writeJson ($filename, $contents) {

  	$file = PATH . STORE . $filename . '.json'; 

		file_put_contents ($file, $contents);

  }


}

new pagespeedjson;

?>