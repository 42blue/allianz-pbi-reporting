<?php

require_once('base.class.php');

class rankingsget extends asebase {

  private $kw_kranken = array('reiserücktrittsversicherung', 'zahnzusatzversicherung', 'private krankenversicherung', 'pflegeversicherung', 'reisekrankenversicherung', 'krankenzusatzversicherung', 'krankentagegeldversicherung', 'krankenhauszusatzversicherung', 'pflegezusatzversicherung', 'private krankenversicherung rechner');

  private $kw_leben = array('berufsunfähigkeitsversicherung', 'private rentenversicherung', 'risikolebensversicherung', 'lebensversicherung', 'riester rente', 'vermögenswirksame leistungen', 'erwerbsminderungsrente', 'betriebliche altersvorsorge', 'rürup rente', 'riester rechner');

  private $kw_sach = array('private haftpflichtversicherung','hausratversicherung','kfz versicherung rechner','teilkasko','e-scooter versicherung','kfz schutzbrief','handyversicherung','urlaub trotz krankschreibung','evb nummer','schadenfreiheitsklassen');

  private $kw_overall = array('reiserücktrittsversicherung', 'zahnzusatzversicherung', 'private krankenversicherung', 'pflegeversicherung', 'reisekrankenversicherung', 'berufsunfähigkeitsversicherung', 'private rentenversicherung', 'risikolebensversicherung', 'lebensversicherung', 'riester rente', 'kfz versicherung rechner', 'kfz versicherung', 'hausrat-haftpflicht-kombi', 'hausratversicherung', 'private haftpflichtversicherung', 'rechtsschutzversicherung', 'unfallversicherung', 'oldtimerversicherung', 'hundekrankenversicherung', 'wohngebäudeversicherung', 'versicherung', 'altersvorsorge', 'krankenzusatzversicherung', 'riesterrente');


  public function __construct () {

    parent::mySqlConnect();

    $this->kw_all = array_unique (array_merge($this->kw_kranken, $this->kw_leben, $this->kw_sach, $this->kw_overall) );

    $this->getSearchConsole();

    $this->getAdwordsDataI18N();

    $this->getRankingsEightDays();

    $this->getRankingsTwelveMonths();    

    $this->rankingsOverall();
    $this->rankingsSparte($this->kw_sach, 'sach');
    $this->rankingsSparte($this->kw_kranken, 'kranken');
    $this->rankingsSparte($this->kw_leben, 'leben');    

    parent::mySqlClose();    

  	echo 'done';

  }


  public function rankingsSparte ($keywords, $name) {

    $result = array();

    $d1 = parent::dateYMDyesterday();
    $d2 = parent::dateYMDoneweekback();

    foreach ($keywords as $keyword) {

      $ra_today = $this->rankingsOverall[$keyword][$d1]['p'];
      $ra_lastw = $this->rankingsOverall[$keyword][$d2]['p'];

      if ($ur_today == null) {
        $ur_today = '-';
      }

      if ($ra_today == 0) {
        $ra_today = 101;
      }

      if ($ra_lastw == 0) {
        $ra_lastw = 101;
      }

      $result[] = array(
        'Keyword'    => $keyword,        
        'Heute'      => intval ($ra_today),
        'Vorwoche'   => intval ($ra_lastw),
        'Change'     => intval ($ra_lastw - $ra_today),
      );

    }

    $json = json_encode($result, JSON_PRETTY_PRINT);

    $jsonfinal = array('results' => $json);

    $this->writeJson('rankings-'.$name, $jsonfinal);

  }



  public function rankingsOverall () {

    $result = array();

    $d1 = parent::dateYMDyesterday();
    $d2 = parent::dateYMDoneweekback();

    foreach ($this->kw_overall as $keyword) {

      $sum = array_sum($this->rankingsAverage[$keyword]);
      $avg = round($sum / count($this->rankingsAverage[$keyword]), 2);

      $ra_today = $this->rankingsOverall[$keyword][$d1]['p'];
      $ra_lastw = $this->rankingsOverall[$keyword][$d2]['p'];
      $ur_today = $this->rankingsOverall[$keyword][$d1]['u'];

      if ($ur_today == null) {
        $ur_today = '-';
      }

      if ($ra_today == 0) {
        $ra_today = 101;
      }

      if ($ra_lastw == 0) {
        $ra_lastw = 101;
      }

      $result[] = array(
        'Keyword'             => $keyword,
        'URL'                 => $ur_today,
        'Rank heute'          => intval ($ra_today),
        'Rank Vorwoche'       => intval ($ra_lastw),
        'Veränderung'         => intval ($ra_lastw - $ra_today),
        'Ø Ranking 12 Monate' => floatval ($avg),
        'SV'                  => intval ($this->adwords[$keyword]),
        'Klicks'              => intval ($this->searchconsole[$keyword])
      );

    }

    $json = json_encode($result, JSON_PRETTY_PRINT);

    $jsonfinal = array('results' => $json);

    $this->writeJson('rankings-all', $jsonfinal);

  }


  public function getRankingsEightDays () {

    $comma_separated_kws = implode('", "', $this->kw_all);
    $comma_separated_kws = '"' . $comma_separated_kws . '"';

    $d1 = parent::dateYMDyesterday();
    $d2 = parent::dateYMDoneweekback();

    $sql = "SELECT
              keyword,
              timestamp,
              language,
              id
            FROM
              ruk_scrape_keywords
            WHERE
              keyword IN ($comma_separated_kws)
            AND
              language = 'de'
            AND (timestamp = '$d1' OR timestamp = '$d2')
            ORDER BY timestamp DESC";

    $result = $this->db->query($sql);

    $rows_kw = array();

    while ($row = $result->fetch_assoc()) {
      $rows_kw[$row['id']] = $row;
    }

    if (empty($rows_kw)) {
      return;
    }

    $rows_kw_keys = implode(',', array_keys($rows_kw));
    
    $sql = "SELECT
              id,
              position,
              url,
              id_kw,
              hostname
            FROM
              ruk_scrape_rankings
            WHERE
              id_kw IN ($rows_kw_keys)";

    $result2 = $this->db->query($sql);

    $rows_ra = array();

    if ($result2->num_rows < 1) {
      return;
    }

    while ($row = $result2->fetch_assoc()) {

      if ($row['hostname'] == 'allianz.de') {

        $row['url'] = strtolower($row['url']);

        $kw = $rows_kw[$row['id_kw']]['keyword'];
        $ts = $rows_kw[$row['id_kw']]['timestamp'];

        $rows_ra[$kw][$ts] = array(
          'k' => $kw,
          't' => $ts,
          'p' => $row['position'],
          'u' => $row['url']
        );
      }

    }


    $this->rankingsOverall = $rows_ra;

  }


  public function getRankingsTwelveMonths () {

    $comma_separated_kws = implode('", "', $this->kw_all);
    $comma_separated_kws = '"' . $comma_separated_kws . '"';

    $d1 = parent::dateYMDyesterday();
    $d2 = parent::dateYMDoneyearkback();

    $sql = "SELECT
              keyword,
              timestamp,
              language,
              id
            FROM
              ruk_scrape_keywords
            WHERE
              keyword IN ($comma_separated_kws)
            AND
              language = 'de'
            AND
              DATE(timestamp) > CURDATE() - INTERVAL 366 DAY
            AND
              DATE(timestamp) < CURDATE() + INTERVAL 1 DAY
            ORDER BY timestamp DESC";

    $result = $this->db->query($sql);

    $rows_kw = array();

    while ($row = $result->fetch_assoc()) {
      $rows_kw[$row['id']] = $row;
    }

    if (empty($rows_kw)) {
      return;
    }

    $rows_kw_keys = implode(',', array_keys($rows_kw));
    
    $sql = "SELECT
              id,
              position,
              id_kw,
              hostname
            FROM
              ruk_scrape_rankings
            WHERE
              id_kw IN ($rows_kw_keys)";

    $result2 = $this->db->query($sql);

    $rows_ra = array();

    if ($result2->num_rows < 1) {
      return;
    }

    while ($row = $result2->fetch_assoc()) {

      if ($row['hostname'] == 'allianz.de') {

        $kw = $rows_kw[$row['id_kw']]['keyword'];
        $ts = $rows_kw[$row['id_kw']]['timestamp'];

        $rows_ra[$kw][$ts] = $row['position'];

      }

    }

    $this->rankingsAverage = $rows_ra;

  }



  public function getAdwordsDataI18N ()
  {

    $comma_separated_kws = implode('", "', $this->kw_all);
    $comma_separated_kws = '"' . $comma_separated_kws . '"';

    $sql = "SELECT
              keyword,
              searchvolume
            FROM
              gen_keywords
            WHERE
              keyword IN ($comma_separated_kws)
            AND
              country = 'de'
            AND
              language = 'de'";

    $res = $this->db->query($sql);

    $googleadwords = array();

    while ($row = $res->fetch_assoc()) {
      $googleadwords[$row['keyword']] = $row['searchvolume'];
    }

    $this->adwords = $googleadwords;

  }


  public function getSearchConsole ()
  {

    $comma_separated_kws = implode('", "', $this->kw_all);
    $comma_separated_kws = '"' . $comma_separated_kws . '"';

    $sql = "SELECT
              sum(clicks) AS cl,
              query,
              timestamp
            FROM
              gwt_data
            WHERE
              hostname_id = '43418'
            AND
              query IN ($comma_separated_kws)                  
            AND
              DATE(timestamp) > CURDATE() - INTERVAL 11 DAY
            GROUP BY 
              query";

    $result_gw = $this->db->query($sql);

    $gwt_rows = array();

    while ($row_gw = $result_gw->fetch_assoc()) {

      $gwt_rows[$row_gw['query']] = $row_gw['cl'];

    }

    $this->searchconsole = $gwt_rows;

  }


  public function writeJson ($filename, $contents) {

  	$file = PATH . STORE . $filename . '.json'; 

		$x = file_put_contents ($file, $contents);

  }

}

new rankingsget;

?>