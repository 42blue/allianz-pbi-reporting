<?php

require_once('base.class.php');

class searchconsolejson extends asebase {

  public function __construct () {

    parent::mySqlConnect();

    $data = $this->getSparten();
    $this->jsonSparten($data);

    $data = $this->getDevice();
    $this->jsonDevice($data);

    $data = $this->getCountry();
    $this->jsonCountry($data);

    parent::mySqlClose();

  }


  public function jsonSparten ($data) {

    // LAST YEAR
    foreach ($data as $key => $value) {

      $sparte    = $value[0];
      $clicks    = $value[1];
      $timestamp = $value[2];

			$kw   = date("W", strtotime($timestamp));
      $year = date("Y", strtotime($timestamp));

      if ($kw == '01') {
      	$year = $year + 1;
      }

	    $result[$sparte][] = array('CW' => $kw, 'Jahr' => $year, 'Klicks' => intval($clicks));


    }

    $jsonfinal_all     = array('results' => $result['all']);
    $jsonfinal_leben   = array('results' => $result['leben']);
    $jsonfinal_sach    = array('results' => $result['sach']);
    $jsonfinal_kranken = array('results' => $result['kranken']);


    $json_all     = json_encode($jsonfinal_all, JSON_PRETTY_PRINT);
    $json_leben   = json_encode($jsonfinal_leben, JSON_PRETTY_PRINT);
    $json_sach    = json_encode($jsonfinal_sach, JSON_PRETTY_PRINT);
    $json_kranken = json_encode($jsonfinal_kranken, JSON_PRETTY_PRINT);
    
    $this->writeJson('searchconsole-all', $json_all);
		$this->writeJson('searchconsole-leben', $json_leben);
		$this->writeJson('searchconsole-sach', $json_sach);
		$this->writeJson('searchconsole-kranken', $json_kranken);

  }



  public function jsonDevice ($data) {

		$calc   = array();
  	$result = array();
  	
		$combined = 0;



  	foreach ($data as $key => $arr) {

  		$device = $arr[0];
  		$share  = $arr[1];
  		$combined = $combined + $share;

			$calc[$device] = $share;

  	}

  	foreach ($calc as $de => $va) {
  		$result['results'][] = array('Device' => $de, 'value' => round($va * 100 / $combined, 2));
  	}

		$json = json_encode($result, JSON_PRETTY_PRINT);

		$this->writeJson('searchconsole-device', $json);

  }


  public function jsonCountry ($data) {

  	$countries = array();
  	$countries['deu'] = 'Deutschland';
  	$countries['aut'] = 'Österreich';
  	$countries['che'] = 'Schweiz';
  	$countries['ita'] = 'Italien';
  	$countries['fra'] = 'Frankreich';
  	$countries['esp'] = 'Spanien';
  	$countries['nld'] = 'Niederlande';

		$calc   = array();
  	$result = array();
  	
		$combined = 0;

  	foreach ($data as $key => $arr) {

  		if (isset($countries[$arr[0]])) {
				$country = $countries[$arr[0]];
  		} else {
  			$country = 'Sonstige';
  		}

  		$share  = $arr[1];
  		$combined = $combined + $share;

  		if (isset($calc[$country])) {
				$calc[$country] = $calc[$country] + $share;
  		} else {
				$calc[$country] = $share;
  		}
		
  	}

  	foreach ($calc as $co => $va) {
  		$result['results'][] = array('Country' => $co, 'value' => round($va * 100 / $combined, 2));
  	}
  
		$json = json_encode($result, JSON_PRETTY_PRINT);

		$this->writeJson('searchconsole-country', $json);

  }


  public function getSparten () {

  	$monday = parent::dateYMDmondayoneweek();

    $sql = "SELECT
              sparte,
              clicks,
              timestamp
            FROM
              aat_searchconsole
            WHERE 
              DATE(timestamp) <= '$monday'";

    $res = $this->db->query($sql);

    $data = array();

    while ($row = $res->fetch_assoc()) {
      $data[] = array ($row['sparte'], $row['clicks'], $row['timestamp']);
    }

    return $data;

  }


  public function getDevice () {

		$monday = parent::dateYMDmondayoneweek();

		$sql = "SELECT
              device,
              clicks,
              timestamp
            FROM
              aat_searchconsole_device
            WHERE 
              DATE(timestamp) = '$monday'";

    $res = $this->db->query($sql);

    $data = array();

    while ($row = $res->fetch_assoc()) {
      $data[] = array ($row['device'], $row['clicks'], $row['timestamp']);
    }

    return $data;

  }


  public function getCountry () {

		$monday = parent::dateYMDmondayoneweek();

		$sql = "SELECT
              country,
              clicks,
              timestamp
            FROM
              aat_searchconsole_country
            WHERE 
              DATE(timestamp) = '$monday'";

    $res = $this->db->query($sql);

    $data = array();

    while ($row = $res->fetch_assoc()) {
      $data[] = array ($row['country'], $row['clicks'], $row['timestamp']);
    }

    return $data;


  }



  public function writeJson ($filename, $contents) {

  	$file = PATH . STORE . $filename . '.json'; 

		file_put_contents ($file, $contents);

  }


}

new searchconsolejson;

?>