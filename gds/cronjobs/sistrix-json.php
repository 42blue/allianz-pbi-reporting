<?php

require_once('base.class.php');

class sistrixjson extends asebase {

  public function __construct () {

    parent::mySqlConnect();

    $data = $this->getSistrix();
    $this->jsonSistrix($data);

    $datas = $this->getSistrixSparten();
    $this->jsonSistrixSparten($datas);


    parent::mySqlClose();

  }


  public function jsonSistrix ($data) {

    foreach ($data as $key => $value) {

      $hostname  = $value[0];
      $score     = number_format($value[1], 2);
      $timestamp = $value[2];

      $result[] = array('cw' => date("W", strtotime($timestamp)), 'host' => $hostname, 'value' => floatval ($score));

    }

    $jsonfinal = array('results' => $result);

    $json = json_encode($result, JSON_PRETTY_PRINT);
    
    $this->writeJson('sistrix-global', $json);

  }



  public function jsonSistrixSparten ($data) {
    
    foreach ($data as $key => $value) {

      $sparte  = $value[0];
      $score     = number_format($value[1], 2);
      $timestamp = $value[2];

      $result[$sparte][] = array('cw' => date("W", strtotime($timestamp)), 'sparte' => $sparte, 'value' => floatval ($score));

    }

    $jsonfinal_leben   = array('results' => $result['leben']);
    $jsonfinal_sach    = array('results' => $result['sach']);
    $jsonfinal_kranken = array('results' => $result['kranken']);

    $json_leben   = json_encode($jsonfinal_leben, JSON_PRETTY_PRINT);
    $json_sach    = json_encode($jsonfinal_sach, JSON_PRETTY_PRINT);
    $json_kranken = json_encode($jsonfinal_kranken, JSON_PRETTY_PRINT);
    
    $this->writeJson('sistrix-leben', $json_leben);
    $this->writeJson('sistrix-sach', $json_sach);
    $this->writeJson('sistrix-kranken', $json_kranken);

  }


  public function getSistrix ()
  {

    $sql = "SELECT
              hostname,
              score,
              timestamp
            FROM
              aat_sistrix_global
            WHERE 
              DATE(timestamp) > CURDATE() - INTERVAL 12 MONTH";

    $res = $this->db->query($sql);

    $data = array();

    while ($row = $res->fetch_assoc()) {
      $data[] = array ($row['hostname'], $row['score'], $row['timestamp']);
    }

    return $data;

  }


  public function getSistrixSparten ()
  {

    $sql = "SELECT
              sparte,
              score,
              timestamp
            FROM
              aat_sistrix_sparten
            WHERE 
              DATE(timestamp) > CURDATE() - INTERVAL 12 MONTH";

    $res = $this->db->query($sql);

    $data = array();

    while ($row = $res->fetch_assoc()) {
      $data[] = array ($row['sparte'], $row['score'], $row['timestamp']);
    }

    return $data;

  }


  public function writeJson ($filename, $contents) {

  	$file = PATH . STORE . $filename . '.json'; 

		file_put_contents ($file, $contents);

  }


}

new sistrixjson;

?>