<?php


require_once('base.class.php');

class pagespeedget extends asebase {

	private $api_ept = 'https://www.webpagetest.org/runtest.php?runs=5&location=ec2-eu-central-1.custom&bwDown=5000&bwUp=1000&latency=70&plr=0&private=0&f=xml';
	private $api_key = 'A.e939f4629c1ab9dee0878e3f0294ea88';

  public $checks = array(
      'Startseite' => 'www.allianz.de',
      'Produktseite' => 'www.allianz.de/auto/kfz-versicherung/',
      'Kampagnen Landingpage' => 'www.allianz.de/angebot/vorsorge/berufsunfaehigkeit/'
  );


  public function __construct () {

    $this->getPageSpeedApi();
       
  }


  private function getPageSpeedApi ()
  {

    foreach ($this->checks as $name => $url) {

			$date = parent::dateYMD();
			
    	$urlc = 'https://' . $url;

    	$call     = $this->api_ept . '&url=' . urlencode($urlc) . '&k=' . $this->api_key;
    	$ctx      = stream_context_create(array('http' => array( 'timeout' => 120 )));
			$response = file_get_contents($call, false, $ctx);

			if (empty($response)) {
        parent::logToFile(parent::timeStamp() . ' WEBPAGETEST Error');
				exit;
			}

      $this->storeResponse($response, $name);

    }

  }


  public function storeResponse ($content, $filename) {

    $filename = strtolower($filename);
    $filename = str_replace(' ', '-', $filename);

    $file = PATH . '/cronjobs/webpagetest-api/' . $filename . '.txt';

    $fh = fopen($file, 'w');
    fwrite($fh, $content);
    fclose($fh);

  }


}

new pagespeedget;

?>
