<?php

require_once('base.class.php');

class sistrixget extends asebase {

  public $hosts  = array('allianz.de', 'ergo.de', 'huk.de', 'cosmosdirekt.de');

  public $sparte = array(
      'leben'   => array('https://www.allianz.de/vorsorge/'), 
      'kranken' => array('https://www.allianz.de/gesundheit/', 'https://www.allianz.de/reise-und-freizeit/'), 
      'sach'    => array('https://www.allianz.de/auto/', 'https://www.allianz.de/recht-und-eigentum/') 
    );


  public function __construct () {

    parent::mySqlConnect();

    $date = parent::dateYMD();

    $this->getSistrixSparte($date);
    $this->getSistrixData($date);

/*
    $startDate =  strtotime('first monday 2019-01');
    $endDate   = strtotime('last monday');

    for ($i = $startDate; $i <= $endDate; $i = strtotime('+1 week', $i)) {
      $date = date("Y-m-d", $i);
      $this->getSistrixSparte($date);
    }
*/

    parent::mySqlClose();

  }


  private function getSistrixSparte ($date)
  {

    foreach ($this->sparte as $sparte => $verz) {

      $score = 0;

      foreach ($verz as $path) {

        $ctx  = stream_context_create(array('http' => array('timeout' => 3)));
        $data = file_get_contents('https://api.sistrix.com/domain.sichtbarkeitsindex?api_key=uaKPBAUKDfajBAzIGjpCMjbsmFzZtKIC&date='.$date.'&path='.$path, false, $ctx);

        $xml = simplexml_load_string($data);

        foreach($xml->answer->sichtbarkeitsindex as $item) {
          $score = $score + floatval($item['value']);
        }

      }

      $sql_ra = "('".$sparte."', '".$score."', '".$date."')";

      $query_ra = 'INSERT INTO aat_sistrix_sparten (sparte, score, timestamp) VALUES '. $sql_ra .' ';

      $call = $this->db->query($query_ra);

    }

  }



  private function getSistrixData ($date)
  {

    foreach ($this->hosts as $host) {

      $ctx  = stream_context_create(array('http' => array('timeout' => 3)));
      $data = file_get_contents('https://api.sistrix.com/domain.sichtbarkeitsindex?api_key=uaKPBAUKDfajBAzIGjpCMjbsmFzZtKIC&date='.$date.'&domain=' . $host, false, $ctx);

      $xml = simplexml_load_string($data);


      if (empty($data)) {
        parent::logToFile(parent::timeStamp() . ' SISTRIX INDEXWATCH Error - No DATA: ' . $host);
        continue;
      }

      if (empty($xml)) {
        parent::logToFile(parent::timeStamp() . ' SISTRIX INDEXWATCH Error - Not XML: ' . $host);
        continue;
      }

      foreach($xml->answer->sichtbarkeitsindex as $item) {

        $score = $item['value'];

        $sql_ra = "('".$host."', '".$score."', '".$date."')";

        $query_ra = 'INSERT INTO
                         aat_sistrix_global (hostname, score, timestamp)
                      VALUES
                        '. $sql_ra .' ';

        $call = $this->db->query($query_ra);

        if (!empty($this->db->error)) {
          parent::logToFile(parent::timeStamp() . ' SISTRIX INDEXWATCH DATA: DB ERROR: ' . $this->db->error);
        }

      }

    }

  }


}

new sistrixget;

?>
