<?php

require_once('base.class.php');

class pagespeedjson extends asebase {

  public function __construct () {

    parent::mySqlConnect();

    $data = $this->getPageSpeed();
    
    $this->jsonPageSpeed($data);
    $this->jsonPageSpeedDetail($data);

    parent::mySqlClose();

  }


  public function jsonPageSpeed ($data) {

    $result = array();

    foreach ($data as $key => $value) {

      $name  = $value['name'];
      $url   = $value['url'];
      $fmp   = $value['fmp'];
      $ts    = $value['timestamp'];

      $site = $name . ': ' . $url;

      $key = 'KW ' . date("W", strtotime($ts)) . ' - ' . date("Y", strtotime($ts));

      if (isset($result[$key])) {
        
        $result[$key][$site] = floatval ($fmp);

      } else {

        $result[$key] = array($site => floatval ($fmp));

      }

    }

    $json = json_encode($result, JSON_PRETTY_PRINT);
    
    $this->writeJson('pagespeed-fmp', $json);

  }


  public function jsonPageSpeedDetail ($data) {

    $startDate  = strtotime('monday -2 week');
    $endDate    = strtotime('last monday');
    $first_date = date("Y-m-d", $endDate);
    $last_date  = date("Y-m-d", $startDate);

    krsort($data);

    $result = array();

    foreach ($data as $key => $value) {

      $name  = $value['name'];
      $url   = $value['url'];
      $ttfb  = $value['ttfb'];
      $fmp   = $value['fmp'];
      $tti   = $value['tti'];
      $ts    = $value['timestamp'];

      $site = $name . ': ' . $url;

      if ($value['timestamp'] == $first_date) {
       
        $result[$site] = array(
          '\u00d8 Time To First Byte - aktuell'     => round(floatval($ttfb), 2),
          '\u00d8 TTFB - Vorwoche'                  => 0,
          '\u00d8 First Meaningful Paint - aktuell' => round(floatval($fmp), 2),
          '\u00d8 FMP - Vorwoche'                   => 0,
          '\u00d8 Time To Interactive - aktuell'    => round(floatval($tti), 2),
          '\u00d8 TTI - Vorwoche'                   => 0
        );

      }

      if ($value['timestamp'] == $last_date) {

        $result[$site]['\u00d8 TTFB - Vorwoche'] = round(floatval($ttfb), 2);
        $result[$site]['\u00d8 FMP - Vorwoche']  = round(floatval($fmp), 2);
        $result[$site]['\u00d8 TTI - Vorwoche']  = round(floatval($tti), 2);

      }

    }

    $json = json_encode($result, JSON_PRETTY_PRINT);
    
    $this->writeJson('pagespeed-detail', $json);

  }


  public function getPageSpeed ()
  {

    $sql = "SELECT 
              *
            FROM
              aat_pagespeed
            WHERE 
              DATE(timestamp) > CURDATE() - INTERVAL 12 MONTH";

    $res = $this->db->query($sql);

    $data = array();

    while ($row = $res->fetch_assoc()) {
      $data[] = $row;
    }

    return $data;

  }


  public function writeJson ($filename, $contents) {

  	$file = PATH . STORE . $filename . '.json'; 

		file_put_contents ($file, $contents);

  }


}

new pagespeedjson;

?>