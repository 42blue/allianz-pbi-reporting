<?php

require_once('base.class.php');

class searchconsolejson extends asebase {

  public function __construct () {

    parent::mySqlConnect();

    $data = $this->getSparten();
    $this->jsonSparten($data);

    $data = $this->getDevice();
    $this->jsonDevice($data);

    $data = $this->getCountry();
    $this->jsonCountry($data);

    parent::mySqlClose();

  }


  public function jsonSparten ($data) {

  	$year_last = date("Y", strtotime('last year'));
    $year_current = date("Y");

    // LAST YEAR
    foreach ($data as $key => $value) {

      $sparte    = $value[0];
      $clicks    = $value[1];
      $timestamp = $value[2];

			$kw   = date("W", strtotime($timestamp));
      $year = date("Y", strtotime($timestamp));
    
      if ($year == $year_last) {

				$key = 'KW ' . $kw . ' - ' . $year;

	      if (isset($result[$sparte][$key])) {
	        
	        $$result_vorjahr[$sparte][$key]['Klicks'] = intval($clicks);

	      } else {

	        $result_vorjahr[$sparte][$key] = array('Klicks' => intval($clicks));

	      }

      }

    }


    // ACUTAL YEAR
    foreach ($data as $key => $value) {

      $sparte    = $value[0];
      $clicks    = $value[1];
      $timestamp = $value[2];

			$kw   = date("W", strtotime($timestamp));
      $year = date("Y", strtotime($timestamp));

      if ($year != $year_current) {

      } else {

	      $key  = 'KW ' . $kw . ' - ' . $year;
	      $keyl = 'KW ' . $kw . ' - ' . $year_last;

	      if (!isset($result_vorjahr[$sparte][$keyl]['Klicks'])) {
	      	$result_vorjahr[$sparte][$keyl]['Klicks'] = 0;
	      }

	      if (isset($result[$sparte][$key])) {
	        
	        $result[$sparte][$key]['Klicks'] = intval($clicks);
	        $result[$sparte][$key]['Klicks Vorjahr'] = $result_vorjahr[$sparte][$keyl]['Klicks'];

	      } else {

	        $result[$sparte][$key] = array('Klicks' => intval($clicks), 'Klicks Vorjahr' => $result_vorjahr[$sparte][$keyl]['Klicks']);

	      }

      }

    }


    $json_all     = json_encode($result['all'], JSON_PRETTY_PRINT);
    $json_leben   = json_encode($result['leben'], JSON_PRETTY_PRINT);
    $json_sach    = json_encode($result['sach'], JSON_PRETTY_PRINT);
    $json_kranken = json_encode($result['kranken'], JSON_PRETTY_PRINT);
    
    $this->writeJson('searchconsole-all', $json_all);
		$this->writeJson('searchconsole-leben', $json_leben);
		$this->writeJson('searchconsole-sach', $json_sach);
		$this->writeJson('searchconsole-kranken', $json_kranken);

  }



  public function jsonDevice ($data) {

		$calc   = array();
  	$result = array();
  	
		$combined = 0;

  	foreach ($data as $key => $arr) {

  		$device = $arr[0];
  		$share  = $arr[1];
  		$combined = $combined + $share;

			$calc[$device] = $share;

  	}

  	foreach ($calc as $de => $va) {
  		$result['Anteil Device'][$de] = round($va * 100 / $combined, 2);
  	}


		$json = json_encode($result, JSON_PRETTY_PRINT);

		$this->writeJson('searchconsole-device', $json);

  }


  public function jsonCountry ($data) {

  	$countries = array();
  	$countries['deu'] = 'Deutschland';
  	$countries['aut'] = 'Österreich';
  	$countries['che'] = 'Schweiz';
  	$countries['ita'] = 'Italien';
  	$countries['fra'] = 'Frankreich';
  	$countries['esp'] = 'Spanien';
  	$countries['nld'] = 'Niederlande';

		$calc   = array();
  	$result = array();
  	
		$combined = 0;

  	foreach ($data as $key => $arr) {

  		if (isset($countries[$arr[0]])) {
				$country = $countries[$arr[0]];
  		} else {
  			$country = 'Sonstige';
  		}

  		$share  = $arr[1];
  		$combined = $combined + $share;

  		if (isset($calc[$country])) {
				$calc[$country] = $calc[$country] + $share;
  		} else {
				$calc[$country] = $share;
  		}
		
  	}

  	foreach ($calc as $co => $va) {
  		$result[$co]['Anteil'] = round($va * 100 / $combined, 2);
  	}
  

		$json = json_encode($result, JSON_PRETTY_PRINT);

		$this->writeJson('searchconsole-country', $json);

  }


  public function getSparten () {

  	$monday = parent::dateYMDmondayoneweek();

    $sql = "SELECT
              sparte,
              clicks,
              timestamp
            FROM
              aat_searchconsole
            WHERE 
              DATE(timestamp) <= '$monday'";

    $res = $this->db->query($sql);

    $data = array();

    while ($row = $res->fetch_assoc()) {
      $data[] = array ($row['sparte'], $row['clicks'], $row['timestamp']);
    }

    return $data;

  }


  public function getDevice () {

		$monday = parent::dateYMDmondayoneweek();

		$sql = "SELECT
              device,
              clicks,
              timestamp
            FROM
              aat_searchconsole_device
            WHERE 
              DATE(timestamp) <= '$monday'";

    $res = $this->db->query($sql);

    $data = array();

    while ($row = $res->fetch_assoc()) {
      $data[] = array ($row['device'], $row['clicks'], $row['timestamp']);
    }

    return $data;

  }


  public function getCountry () {

		$monday = parent::dateYMDmondayoneweek();

		$sql = "SELECT
              country,
              clicks,
              timestamp
            FROM
              aat_searchconsole_country
            WHERE 
              DATE(timestamp) = '$monday'";

    $res = $this->db->query($sql);

    $data = array();

    while ($row = $res->fetch_assoc()) {
      $data[] = array ($row['country'], $row['clicks'], $row['timestamp']);
    }

    return $data;


  }



  public function writeJson ($filename, $contents) {

  	$file = PATH . STORE . $filename . '.json'; 

		file_put_contents ($file, $contents);

  }


}

new searchconsolejson;

?>