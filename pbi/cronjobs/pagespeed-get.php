<?php


require_once('base.class.php');

class pagespeedget extends asebase {

	private $api_ept = 'https://www.googleapis.com/pagespeedonline/v5/';
	private $api_key = 'AIzaSyAEDDoCN3WfAVZa0J9KLsVyxQWDnIexnmY';

  public $checks = array(
      'Startseite' => 'www.allianz.de',
      'Produktseite' => 'www.allianz.de/auto/kfz-versicherung/',
      'Kampagnen Landingpage' => 'www.allianz.de/angebot/vorsorge/berufsunfaehigkeit/'
  );


  public function __construct () {

    $this->getPageSpeedApi();
       
  }


  private function getPageSpeedApi ()
  {

    foreach ($this->checks as $name => $url) {

			$date = parent::dateYMD();
			
    	$urlc = 'https://' . $url;

    	$ttfb = $this->getTTFB($urlc);

    	$call     = $this->api_ept . 'runPagespeed?url=' . urlencode($urlc) . '&key=' . $this->api_key . '&key=&strategy=desktop&locale=de';
    	$ctx      = stream_context_create(array('http' => array( 'timeout' => 120 )));
			$response = file_get_contents($call, false, $ctx);

			$data = json_decode($response, true);

			if (empty($response) || $data == false) {
        parent::logToFile(parent::timeStamp() . ' PAGESPEED Error');
				exit;
			}

			$score = $data['lighthouseResult']['audits']['speed-index']['score'] * 100;
			$fmp   = $data['lighthouseResult']['audits']['first-meaningful-paint']['score'];
			$tti   = $data['lighthouseResult']['audits']['interactive']['score'];

			parent::mySqlConnect();
      
      $sql_ra = "('".$name."', '".$url."', '".$ttfb."', '".$fmp."', '".$tti."', '".$score."', '".$date."')";

      $query_ra = 'INSERT INTO
                       aat_pagespeed (name, url, ttfb, fmp, tti, score, timestamp)
                    VALUES
                      '. $sql_ra .' ';

      $sql = $this->db->query($query_ra);

    	parent::mySqlClose();

    }

  }


  private function getTTFB ($url) {

		$ch = curl_init();

		curl_setopt($ch, CURLOPT_URL, $url);
		curl_setopt($ch, CURLOPT_HEADER, 0);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, 0);
		curl_setopt($ch, CURLOPT_NOBODY, 1);
		curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 0);

		curl_exec($ch);
		$info = curl_getinfo($ch);
		curl_close($ch);

		return round($info['starttransfer_time'], 2);

  }

}

new pagespeedget;

?>
