<?php

require_once('base.class.php');

class sistrixjson extends asebase {

  public function __construct () {

    parent::mySqlConnect();

    $data = $this->getSistrix();
    $this->jsonSistrix($data);

    $datas = $this->getSistrixSparten();
    $this->jsonSistrixSparten($datas);


    parent::mySqlClose();

  }


  public function jsonSistrix ($data) {

    foreach ($data as $key => $value) {

      $hostname  = $value[0];
      $score     = number_format($value[1], 2);
      $timestamp = $value[2];

      $key = 'KW ' . date("W", strtotime($timestamp)) . ' - ' . date("Y", strtotime($timestamp));

      if (isset($result[$key])) {
        
        $result[$key][$hostname] = floatval ($score);

      } else {

        $result[$key] = array($hostname => floatval ($score));

      }

    }

    $json = json_encode($result, JSON_PRETTY_PRINT);
    
    $this->writeJson('sistrix-global', $json);

  }



  public function jsonSistrixSparten ($data) {

    foreach ($data as $key => $value) {

      $sparte  = $value[0];
      $score     = number_format($value[1], 2);
      $timestamp = $value[2];

      $key = 'KW ' . date("W", strtotime($timestamp)) . ' - ' . date("Y", strtotime($timestamp));

      if (isset($result[$sparte][$key])) {
        
        $result[$sparte][$key][$sparte] = floatval ($score);

      } else {

        $result[$sparte][$key] = array($sparte => floatval ($score));

      }

    }


    $json_leben = json_encode($result['leben'], JSON_PRETTY_PRINT);
    $json_sach = json_encode($result['sach'], JSON_PRETTY_PRINT);
    $json_kranken = json_encode($result['kranken'], JSON_PRETTY_PRINT);
    
    $this->writeJson('sistrix-leben', $json_leben);
    $this->writeJson('sistrix-sach', $json_sach);
    $this->writeJson('sistrix-kranken', $json_kranken);

  }


  public function getSistrix ()
  {

    $sql = "SELECT
              hostname,
              score,
              timestamp
            FROM
              aat_sistrix_global
            WHERE 
              DATE(timestamp) > CURDATE() - INTERVAL 12 MONTH";

    $res = $this->db->query($sql);

    $data = array();

    while ($row = $res->fetch_assoc()) {
      $data[] = array ($row['hostname'], $row['score'], $row['timestamp']);
    }

    return $data;

  }


  public function getSistrixSparten ()
  {

    $sql = "SELECT
              sparte,
              score,
              timestamp
            FROM
              aat_sistrix_sparten
            WHERE 
              DATE(timestamp) > CURDATE() - INTERVAL 12 MONTH";

    $res = $this->db->query($sql);

    $data = array();

    while ($row = $res->fetch_assoc()) {
      $data[] = array ($row['sparte'], $row['score'], $row['timestamp']);
    }

    return $data;

  }


  public function writeJson ($filename, $contents) {

  	$file = PATH . STORE . $filename . '.json'; 

		file_put_contents ($file, $contents);

  }


}

new sistrixjson;

?>