<?php

require_once('base.class.php');

class rankingsoverallget extends asebase {

  public function __construct () {

    parent::mySqlConnect();

    $this->getRankingsToday();

    $this->rankingsSparte();

    parent::mySqlClose();    

  	echo 'done';

  }


  public function rankingsSparte () {

    $data = $this->rankings;

    $result_all     = 0;
    $result_leben   = 0;
    $result_sach    = 0;
    $result_kranken = 0;
    $result_brand_leben   = 0;
    $result_brand_sach    = 0;
    $result_brand_kranken = 0;

    // LEBEN   = /vorsorge/
    // SACH    = /auto/ und /recht-und-eigentum/
    // KRANKEN =  /gesundheit/ und /reise-und-freizeit

    foreach ($data as $set) {

      foreach ($set as $keyword => $rank) {

        $result_all++;

        if ($rank['p'] < 11) {
          if (stripos($rank['u'], '/vorsorge/') !== false) {
            $result_leben++;
            if (stripos($rank['k'], 'allianz') !== false) {
              $result_brand_leben++;
            }
          }
          if (stripos($rank['u'], '/gesundheit/') !== false || stripos($rank['u'], '/reise-und-freizeit/') !== false ) {
            $result_kranken++;
            if (stripos($rank['k'], 'allianz') !== false) {
              $result_brand_kranken++;
            }            
          }
          if (stripos($rank['u'], '/auto/') !== false || stripos($rank['u'], '/recht-und-eigentum/') !== false ) {
            $result_sach++;
            if (stripos($rank['k'], 'allianz') !== false) {
              $result_brand_sach++;
            }            
          }
        }

      }

    }


    $json_leben   = json_encode(array('Anzahl Top 10 Rankings' => intval($result_leben)));
    $json_sach    = json_encode(array('Anzahl Top 10 Rankings' => intval($result_sach)));
    $json_kranken = json_encode(array('Anzahl Top 10 Rankings' => intval($result_kranken)));

    $this->writeJson('rankings-leben-overall', $json_leben);
    $this->writeJson('rankings-sach-overall', $json_sach);
    $this->writeJson('rankings-kranken-overall', $json_kranken);


    
    $json_distribution_leben   = $result_brand_leben * 100 / $result_leben;
    $json_distribution_sach    = $result_brand_sach * 100 / $result_sach;
    $json_distribution_kranken = $result_brand_kranken * 100 / $result_kranken;

    $json_distribution_leben   = round($json_distribution_leben, 2);
    $json_distribution_sach    = round($json_distribution_sach, 2);
    $json_distribution_kranken = round($json_distribution_kranken, 2);

    $json_distribution_leben   = array ('Brand' => array('Leben' => $json_distribution_leben), 'Non Brand' => array('Leben' => 100-$json_distribution_leben));
    $json_distribution_sach    = array ('Brand' => array('Sach' => $json_distribution_sach), 'Non Brand' => array('Sach' => 100-$json_distribution_sach));
    $json_distribution_kranken = array ('Brand' => array('Kranken' => $json_distribution_kranken), 'Non Brand' => array('Kranken' => 100-$json_distribution_kranken));

    $this->writeJson('rankings-leben-distribution', json_encode($json_distribution_leben));
    $this->writeJson('rankings-sach-distribution', json_encode($json_distribution_sach));
    $this->writeJson('rankings-kranken-distribution', json_encode($json_distribution_kranken));

  }


  public function getRankingsToday () {

    $d1 = parent::dateYMDyesterday();

    $sql = "SELECT
              keyword,
              timestamp,
              language,
              id
            FROM
              ruk_scrape_keywords
            WHERE
              language = 'de'
            AND timestamp = '$d1'";

    $result = $this->db->query($sql);

    $rows_kw = array();

    while ($row = $result->fetch_assoc()) {
      $rows_kw[$row['id']] = $row;
    }

    if (empty($rows_kw)) {
      return;
    }

    $rows_kw_keys = implode(',', array_keys($rows_kw));
    
    $sql = "SELECT
              id,
              position,
              url,
              id_kw,
              hostname
            FROM
              ruk_scrape_rankings
            WHERE
              id_kw IN ($rows_kw_keys)
            ";

    $result2 = $this->db->query($sql);

    $rows_ra = array();

    if ($result2->num_rows < 1) {
      return;
    }

    while ($row = $result2->fetch_assoc()) {

      if ($row['hostname'] == 'allianz.de') {

        $kw = $rows_kw[$row['id_kw']]['keyword'];

        $rows_ra[$row['id_kw']][] = array(
          'k' => $kw,          
          'p' => $row['position'],
          'u' => $row['url']
        );
      }

    }


    $this->rankings = $rows_ra;

    //$data = serialize($rows_ra);
    //file_put_contents($fn, $data);

  }


  public function readFile ()
  {

    $fileName = PATH . STORE . 'aaa.temp';

    if (file_exists($fileName)) {

      $handle   = fopen($fileName, 'rb');
      $variable = fread($handle, filesize($fileName));
      fclose($handle);

      $variable = unserialize($variable);

      return $variable;

    } else {

      return null;

    }

  }


  public function writeJson ($filename, $contents) {

  	$file = PATH . STORE . $filename . '.json'; 

		$x = file_put_contents ($file, $contents);

  }

}

new rankingsoverallget;

?>